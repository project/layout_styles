CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Layout Styles allows site builders to add a image which is added as a
background image to layout builder sections.
In addition to this it also provides a easy way to add a  class to the layout
builder sections.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/layout_styles/

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/layout_styles/


REQUIREMENTS
------------

* This module requires, at minimum, Drupal 8.7.0.
* This module requires no additional modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

When adding a section in the layout, this module will provide 2 additional
fields background image and add class. The image added in the background image
field willadded as a background image using inline styling.

It also provides an option to add the class to the section using Add class 
field.
Multiple classes can also be provide using this field.
