<?php

namespace Drupal\layout_styles\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\file\Entity\File;

/**
 * {@inheritdoc}
 */
class LayoutBase extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'background_image' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['background_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Background Image'),
      '#default_value' => $this->configuration['background_image'],
      '#upload_location' => 'public://',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
    ];
    $form['layout_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add class'),
      '#default_value' => isset($this->configuration['layout_class']) ? $this->configuration['layout_class'] : '',
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['background_image'] = $form_state->getValue('background_image');
    $this->configuration['layout_class'] = $form_state->getValue('layout_class');
    if (!empty($this->configuration['background_image'][0])) {
      $fid = $this->configuration['background_image'][0];
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    if (!empty($this->configuration['background_image'][0])) {
      $fid = $this->configuration['background_image'][0];
      $file = File::load($fid);
      $file_uri = $file->getFileUri();
      $url = file_create_url($file_uri);
      $build['#attributes']['style'] = 'background-image: url(' . $url . ')';
    }
    if (!empty($this->configuration['layout_class'])) {
      $build['#attributes']['class'][] = $this->configuration['layout_class'];
    }
    return $build;
  }

}
