<?php

namespace Drupal\layout_styles\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\file\Entity\File;

/**
 * Base class of layouts with configurable widths.
 *
 * @internal
 *   Plugin classes are internal.
 */
abstract class MultipleWidthLayoutBase extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $width_classes = array_keys($this->getWidthOptions());
    return $configuration + [
      'column_widths' => array_shift($width_classes),
      'background_image' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['column_widths'] = [
      '#type' => 'select',
      '#title' => $this->t('Column widths'),
      '#default_value' => $this->configuration['column_widths'],
      '#options' => $this->getWidthOptions(),
      '#description' => $this->t('Choose the column widths for this layout.'),
    ];
    $form['background_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Background Image'),
      '#default_value' => $this->configuration['background_image'],
      '#upload_location' => 'public://',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
    ];
    $form['layout_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add class'),
      '#default_value' => isset($this->configuration['layout_class']) ? $this->configuration['layout_class'] : '',
      '#description' => $this->t('Add the class for this section.'),
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['column_widths'] = $form_state->getValue('column_widths');
    $this->configuration['background_image'] = $form_state->getValue('background_image');
    $this->configuration['layout_class'] = $form_state->getValue('layout_class');
    if (!empty($this->configuration['background_image'][0])) {
      $fid = $this->configuration['background_image'][0];
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    if (!empty($this->configuration['background_image'][0])) {
      $fid = $this->configuration['background_image'][0];
      $file = File::load($fid);
      $file_uri = $file->getFileUri();
      $url = file_create_url($file_uri);
      $build['#attributes']['style'] = 'background-image: url(' . $url . ')';
    }
    $build['#attributes']['class'] = [
      'layout',
      $this->getPluginDefinition()->getTemplate(),
      $this->getPluginDefinition()->getTemplate() . '--' . $this->configuration['column_widths'],
    ];
    if (!empty($this->configuration['layout_class'])) {
      $build['#attributes']['class'][] = $this->configuration['layout_class'];
    }
    return $build;
  }

  /**
   * Gets the width options for the configuration form.
   *
   * The first option will be used as the default 'column_widths' configuration
   * value.
   *
   * @return string[]
   *   The width options array where the keys are strings that will be added to
   *   the CSS classes and the values are the human readable labels.
   */
  abstract protected function getWidthOptions();

}
